import { shallowMount } from '@vue/test-utils'
const request = require('supertest');
import App from '@/App.vue'

describe('INTEGRATION TESTS', () => {
  describe('"callSum()" function tests.', () => {
    test('When "checkNumbers()" returns "true", "sumNumbers()" must be called', () => {
      const wrapper = shallowMount(App)
      let sumNumbers = jest.spyOn(wrapper.vm, 'sumNumbers')
      
      wrapper.setData({
        firstNum: '2',
        secondNum: '3',
        result: null,
        error: null
      })

      wrapper.vm.callSum()
      
      expect(sumNumbers).toHaveBeenCalled()
    })
    test('When "checkNumbers()" returns "false", "sumNumbers()" must not be called', () => {
      const wrapper = shallowMount(App)
      let sumNumbers = jest.spyOn(wrapper.vm, 'sumNumbers')
      
      wrapper.setData({
        firstNum: '',
        secondNum: '',
        result: null,
        error: null
      })

      wrapper.vm.callSum()

      expect(sumNumbers).not.toHaveBeenCalled()
    })
  })

  describe('API request test.', () => {
    test('When request "todos/1", return statusCode 200 and body with title', async () => {
      const call = await request('https://jsonplaceholder.typicode.com/').get('todos/1').expect(200)
      
      expect(call.statusCode).toEqual(200)
      expect(call.body).toBeDefined()
      expect(call.body.title).toBe('delectus aut autem')
    })
  })
})
