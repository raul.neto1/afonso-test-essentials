import { shallowMount } from '@vue/test-utils'
import App from '@/App.vue'

describe('UNIT TESTS', () => {
  describe('"checkNumbers()" function tests.', () => {
    test('When function recives two numbers returns true', () => {
      const wrapper = shallowMount(App)
      
      const functionResponse = wrapper.vm.checkNumbers('2', '3')
  
      expect(functionResponse).toBeTruthy()
    })

    test('When function does not recives two numbers returns false', () => {
      const wrapper = shallowMount(App)
      
      const functionResponse = wrapper.vm.checkNumbers('2', '')
  
      expect(functionResponse).not.toBeTruthy()
    })

    test('When the function receive strings returns false', () => {
      const wrapper = shallowMount(App)
      
      const functionResponse = wrapper.vm.checkNumbers('test', '3')
  
      expect(functionResponse).not.toBeTruthy()
    })
  })

  describe('"sumNumbers()" function tests.', () => {
    test('When the function receives two numbers "this.result" must be the sum of them', () => {
      const wrapper = shallowMount(App)
      
      wrapper.vm.sumNumbers('2', '3')
      
      const result = wrapper.vm._data.result
  
      expect(result).toEqual(5)
    })

    test('When the function does not receives two numbers "this.result" must be "null"', () => {
      const wrapper = shallowMount(App)
      
      wrapper.vm.sumNumbers('2', 'e2e')
      
      const result = wrapper.vm._data.result
  
      expect(result).toBeNull()
    })
  })
})
