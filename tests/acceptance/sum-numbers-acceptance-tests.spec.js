import puppeteer from 'puppeteer'

describe('ACCEPTANCE TESTS', () => {
  test('When user enters two numbers, their sum should be displayed on page', async () => {
    const firstNum = '20'
    const secondNum = '30'
    const resultNum = '50'

    const browser = await puppeteer.launch()

    const page = await browser.newPage()
    await page.goto('http://localhost:8081/')
    await page.click('input#firstInput')
    await page.type('input#firstInput', firstNum)
    await page.click('input#secondInput')
    await page.type('input#secondInput', secondNum)
    await page.click('#submitButton')
    const result = await page.$eval('.result', el => el.textContent)

    expect(result).toEqual(resultNum)
  })

  test('When user enters an invalid number, an error message should be displayed', async () => {
    const firstNum = '20'
    const secondNum = 'e2e'
    const errorMessage = 'Enter the numbers correctly'

    const browser = await puppeteer.launch()

    const page = await browser.newPage()
    await page.goto('http://localhost:8081/')
    await page.click('input#firstInput')
    await page.type('input#firstInput', firstNum)
    await page.click('input#secondInput')
    await page.type('input#secondInput', secondNum)
    await page.click('#submitButton')
    const error = await page.$eval('.error', el => el.textContent)

    expect(error).toEqual(errorMessage)
  })
})
